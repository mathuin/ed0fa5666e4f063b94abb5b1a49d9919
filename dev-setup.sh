#!/usr/bin/env sh
docker-machine rm -f rancher host1
docker-machine create rancher --driver virtualbox --virtualbox-cpu-count "-1" --virtualbox-disk-size "8000" --virtualbox-memory "512" --virtualbox-boot2docker-url=https://github.com/boot2docker/boot2docker/releases/download/v1.11.2/boot2docker.iso
docker-machine scp scripts/rancher-net.sh rancher:.
docker-machine ssh rancher sh rancher-net.sh
docker-machine regenerate-certs rancher -f
eval $(docker-machine env rancher)
docker-compose up -d
eval $(docker-machine env -u)
docker-machine create host1 --driver virtualbox --virtualbox-cpu-count "-1" --virtualbox-disk-size "54000" --virtualbox-memory "2048" --virtualbox-boot2docker-url=https://github.com/boot2docker/boot2docker/releases/download/v1.11.2/boot2docker.iso
docker-machine scp scripts/host1-net.sh host1:.
docker-machine ssh host1 sh host1-net.sh
docker-machine scp scripts/host1-add.sh host1:.
docker-machine ssh host1 sh host1-add.sh
docker-machine scp host1:api-keys .
