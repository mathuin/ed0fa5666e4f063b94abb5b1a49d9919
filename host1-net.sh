#!/bin/sh
sudo mkdir /mnt/sda1/var/lib/rancher
cp /var/lib/boot2docker/profile .
cat >> ./profile <<PROFILE
sudo cat /var/run/udhcpc.eth1.pid | xargs sudo kill
sudo ifconfig eth1 192.168.99.101 netmask 255.255.255.0 broadcast 192.168.99.255 up
sudo mkdir /var/lib/rancher
sudo mount -r /mnt/sda1/var/lib/rancher /var/lib/rancher
PROFILE
sudo mv ./profile /var/lib/boot2docker/profile
sh /var/lib/boot2docker/profile
