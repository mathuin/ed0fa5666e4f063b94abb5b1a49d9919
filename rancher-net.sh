#!/bin/sh
cp /var/lib/boot2docker/profile ./profile
cat >> ./profile <<PROFILE
sudo cat /var/run/udhcpc.eth1.pid | xargs sudo kill
sudo ifconfig eth1 192.168.99.100 netmask 255.255.255.0 broadcast 192.168.99.255 up
PROFILE
sudo mv ./profile /var/lib/boot2docker/profile
sh /var/lib/boot2docker/profile
